﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicBasics
{
    class Program
    {
        public static dynamic Add(dynamic a, dynamic b)
        {
            return a + b;
        }

        static void Main(string[] args)
        {
            dynamic result = Add(5, 10);

            Console.WriteLine("result = " + result + "; typeof = " + (result.GetType()));

            result = Add("hello", "dynamic");

            Console.WriteLine("result = " + result + "; typeof = " + (result.GetType()));

            result = Add(DateTime.Now, TimeSpan.FromDays(2));

            Console.WriteLine("result = " + result + "; typeof = " + (result.GetType()));
        }
    }
}
