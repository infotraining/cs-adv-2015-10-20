﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DynamicObjects
{
    public class Expando : DynamicObject
    {
        Dictionary<string, object> _objects = new Dictionary<string, object>(); 

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            _objects[binder.Name] = value;

            return true;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return _objects.TryGetValue(binder.Name, out result);            
        }

        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return _objects.Keys;
        }
    }

    class XWrapper : DynamicObject
    {
        private XElement _element;

        public XWrapper(XElement e)
        {
            _element = e;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = _element.Attribute(binder.Name).Value;

            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            _element.SetAttributeValue(binder.Name, value);

            return true;
        }

        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return _element.Attributes().Select(a => a.Name.ToString());
        }
    }

    public static class XExtensions
    {
        public static dynamic DynamicAttributes(this XElement e)
            => new XWrapper(e);

    }

    class Program
    {
        static void ExpandoDemo()
        {
            dynamic bag = new Expando();

            bag.Name = "Zenon";
            Console.WriteLine(bag.Name);

            bag.Id = 7;
            Console.WriteLine(bag.Id);
        }

        static void DynamicXAttributeDemo()
        {
            XElement node = XElement.Parse(@"<Label Text=""Hello"" Id=""5""/>");

            dynamic da = node.DynamicAttributes();

            Console.WriteLine(da.Id);
            Console.WriteLine(da.Text);
        }

        static void Main(string[] args)
        {           
            ExpandoDemo();

            //DynamicXAttributeDemo();
        }
    }
}
