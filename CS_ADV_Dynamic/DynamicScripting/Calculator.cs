﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronPython.Hosting;
using IronPython.Runtime;
using Microsoft.Scripting.Hosting;

namespace DynamicScripting
{
    class Calculator
    {
        static void Main()
        {
            var result = Calculate("[1, 2, 3] + [4, 5]");
            Console.WriteLine(result);       
        }

        static object Calculate(string expression)
        {
            int x = 10;

            ScriptEngine engine = Python.CreateEngine();

            var scope = engine.CreateScope();
            scope.SetVariable("x", x);
                    
            return engine.Execute(expression, scope);
        }
    }
}
