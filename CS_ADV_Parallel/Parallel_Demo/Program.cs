﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Parallel_Demo
{
    class Program
    {
        static void InvokeDemo()
        {
            string[] pages = new string[3];

            Parallel.Invoke(
                () => pages[0] = new WebClient().DownloadString("http://cocomo.pl"),
                () => pages[1] = new WebClient().DownloadString("http://abb.com"),
                () => pages[2] = new WebClient().DownloadString("http://cocomo.pl")
               );

            Console.WriteLine("Start");

            foreach (var p in pages)
            {
                Console.WriteLine(p.Take(100).ToArray());
                Console.WriteLine("\n\n--------------\n\n");
            }
        }

        public static void ForDemo()
        {
            const int SIZE = 100;

            Guid[] guids = new Guid[SIZE];

            Parallel.For(0, SIZE, i => guids[i] = Guid.NewGuid());

            //Parallel.For(0, guids.Length, i => Console.WriteLine("{0}: {1}", i, guids[i]));
            Parallel.ForEach(guids, (g, loopState, i) =>
            {
                if (i == 10)
                    loopState.Stop();

                Console.WriteLine("{0}: {1}", i, g);
            });
        }

        static void Main(string[] args)
        {
            ForDemo();   
        }
    }
}
