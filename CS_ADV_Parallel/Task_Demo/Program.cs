﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Task_Demo
{
    internal class Program
    {
        private static void ChildParent()
        {
            Task t1 = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Start T1");
                Task tchild = Task.Factory.StartNew(() =>
                {
                    Console.WriteLine("Start child");
                    Thread.Sleep(10000);
                    Console.WriteLine("Finished child");
                });
                Thread.Sleep(3000);
                Console.WriteLine("Finish T1");
            });

            t1.Wait();

            Console.WriteLine("End of main");
        }

        private static void Main(string[] args)
        {
            int N = 100;

            List<Task<List<int>>> tasks = new List<Task<List<int>>>(N);

            //TaskFactory<List<int>> factory = new TaskFactory<List<int>>();

            for (int i = 0; i < N/2; ++i)
            {
                int index = i;
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    Console.WriteLine("Task {0} started", index);
                    var results = Enumerable.Range(2, 1000000)
                        .Where(n => Enumerable.Range(2, (int) Math.Sqrt(n) - 1).All(d => n%d != 0)).ToList();
                    Console.WriteLine("Task {0} stopped", index);
                    return results;
                }, TaskCreationOptions.PreferFairness));
            }

            Thread.Sleep(10000);

            Console.WriteLine("\n\nSecond batch");

            for (int i = 0; i < N / 2; ++i)
            {
                int index = i+50;
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    Console.WriteLine("Task {0} started", index);
                    var results = Enumerable.Range(2, 1000000)
                        .Where(n => Enumerable.Range(2, (int)Math.Sqrt(n) - 1).All(d => n % d != 0)).ToList();
                    Console.WriteLine("Task {0} stopped", index);
                    return results;
                }, TaskCreationOptions.PreferFairness));
            }

            Task.WaitAll(tasks.ToArray());
        }


    }
}
