﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PLinq_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            const int N = 10000000;
            Stopwatch stopwatch = new Stopwatch();

            CancellationTokenSource tokenSource = new CancellationTokenSource();
            CancellationToken token = tokenSource.Token;

            stopwatch.Start();

            var primes =
                ParallelEnumerable.Range(2, N - 1).AsParallel().AsOrdered().WithCancellation(token)
                    .Where(n => Enumerable.Range(2, (int) Math.Sqrt(n) - 1).All(d => n%d != 0));

            Task.Run(() =>
            {
                Thread.Sleep(3000);
                tokenSource.Cancel();
            });

            try
            {
                var results = primes.ToList();

                stopwatch.Stop();

                foreach (var prime in results.Take(30))
                    Console.Write(prime + " ");

                Console.WriteLine();
            }
            catch(OperationCanceledException ex)
            {
                Console.WriteLine("Anulowano liczenie liczb pierwszych");
            }

            Console.WriteLine("Time: {0}", stopwatch.Elapsed);

            int[] numbers = {5, 36, 234, 234, 6564, 123, 2, 5, 66, 57};

            var squares = numbers.AsParallel().Select(n => n*n);

            squares.ForAll(n => Console.Write(n + " "));

            //foreach(var s in squares)
            //    Console.WriteLine(s);

            var errorProneCode = Enumerable.Range(1, 10).AsParallel()
                .Select(n =>
                {
                    if (n%2 == 0)
                        throw new ArgumentException(n.ToString() + " is even...");

                    return n;
                });

            try
            {
                foreach (var n in errorProneCode)
                {
                    Console.WriteLine();
                }
            }
            catch (AggregateException ae)
            {
                //ae.Handle((exception) =>
                //{
                //    if (exception is ArgumentException)
                //    {
                //        Console.WriteLine(exception.Message);

                //        return true;
                //    }

                //    return false;
                //});

                foreach (var ex in ae.InnerExceptions)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            var query = Enumerable.Range(0, 100).AsParallel()
                .Aggregate(0.0, (subTotal, n) => subTotal += n*n, (total, subTotal) => total += subTotal,
                    total => Math.Sqrt(total));

            Console.WriteLine("Total: " + query);


            var hosts = new List<string>
            {
                "google.com", "yahoo.com", "abb.com", "cocomo.pl", "nbp.pl", "wp.pl",
                "google.com", "yahoo.com", "abb.com", "cocomo.pl", "nbp.pl", "wp.pl"
            };

            var pages = hosts.AsParallel().WithDegreeOfParallelism(8).Select(h =>
            {
                Console.WriteLine("Starting " + h);
                return new WebClient().DownloadString("http://" + h);
            });

            try
            {
                pages.ForAll((content) =>
                {
                    Console.WriteLine(content.Take(100).ToArray());
                    Console.WriteLine("\n----\n");
                });
            }
            catch (AggregateException ae)
            {
                ae.Handle((e) =>
                {
                    Console.WriteLine(e.Message);
                    return true;
                });
            }
        }
    }
}
