﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncMethod
{
    class Program
    {
        static Task<string> DownloadContentAsync(string url)
        {
            return Task.Run<string>(() => new WebClient().DownloadString(url));
        }

        static async Task SaveAllDataAsync(params string[] urls)
        {
            Task<string>[] tasks = new Task<string>[urls.Length];

            for (int i = 0; i < tasks.Length; ++i)
            {
                Console.WriteLine("Starting downloading: " + urls[i]);
                tasks[i] = DownloadContentAsync(urls[i]);
            }

            Console.WriteLine("Download has started");

            try
            {
                string[] content = await Task.WhenAll(tasks);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType());
                throw e;
            }

            Console.WriteLine("Saving a content: ");
            //for(int i = 0; i < content.Length; ++i)
            //{
            //    string fileName = new Uri(urls[i]).Host + ".txt";

            //    using (StreamWriter writer = new StreamWriter(fileName))
            //    {
            //        Console.WriteLine("Saving: " + fileName);
            //        await writer.WriteAsync(content[i]);
            //        Console.WriteLine("Saved: " + fileName);
            //    }
            //}

            Console.WriteLine("End"); 
        }

        static async Task SaveAnyDataAsync(params string[] urls)
        {
            Task<string>[] tasks = new Task<string>[urls.Length];

            for (int i = 0; i < tasks.Length; ++i)
            {
                Console.WriteLine("Starting downloading: " + urls[i]);
                tasks[i] = DownloadContentAsync(urls[i]);
            }

            Console.WriteLine("Download has started");

            Task<string> contentTask = await Task.WhenAny(tasks);
            string content = await contentTask;

            Console.WriteLine("Saving a content: ");

            string fileName = "data.txt";

            using (StreamWriter writer = new StreamWriter(fileName))
            {
                Console.WriteLine("Saving: " + fileName);
                await writer.WriteAsync(content);
                Console.WriteLine("Saved: " + fileName);
            }
            
            Console.WriteLine("End");
        }

        static async Task MainAsync()
        {
            string[] urls = { "http://www.infotraing.pl", "http://www.infotraing.com", "http://www.gazeta.pl" };

            try
            {
                await SaveAllDataAsync(urls);
            }
            catch (Exception e)
            {
                Console.WriteLine("Caught an exception: " + e.Message);
            }
        }

        static void Main(string[] args)
        {
            var tasks = new List<Task<int>>();

            for (int i = 0; i < 3; ++i)
            {
                int index = i;
                tasks.Add(Task.Run(() =>
                {
                    Console.WriteLine("Start #{0}", index);
                    Thread.Sleep((index + 1) * 2000);

                    if (index == 2)
                        throw new ArgumentException("I don;t like java...");
                            
                    Console.WriteLine("End #{0}", index);

                    return index;
                }));
            }

            var whenAllTask = Task.WhenAll(tasks).ContinueWith((futureInts) =>
            {
                foreach (var item in futureInts.Result)
                    Console.WriteLine(item);
            });

            var results1 = Task.WhenAny(tasks)
                                .ContinueWith(r => Console.WriteLine("First result: " + r.Result.Result));


            try
            {
                whenAllTask.Wait();
            }
            catch (Exception e)
            {
                
                Console.WriteLine("Error: " + e.Message);
            }
            


            //MainAsync().Wait();
        }
    }
}
