﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AsyncExplained
{
    class Program
    {
        const int N = 1000000;

        static Task<int> GetPrimesCountAsync(int start, int count)
        {
            return Task.Run(()
                => ParallelEnumerable
                        .Range(start, count)
                        .Count(n => Enumerable.Range(2, (int)Math.Sqrt(n) - 1)
                            .All(i => n % i > 0)));
        }

        static async void DisplayPrimesCount()
        {
            for(int i = 0; i < 10; ++i)
                Console.WriteLine(await GetPrimesCountAsync(i * N + 2, N - 2) + " primes between " + (i * N + 2) + " and " + (i * N));
            Console.WriteLine("Done");
        }

        async static Task<int> LongCalculation()
        {
            Console.WriteLine("Calculation started");
            await Task.Delay(10000);
            Console.WriteLine("Calculation done");
            return 13;
        }

        async static Task Go()
        {
            Console.WriteLine("Go started");
            int result = await LongCalculation();
            Console.WriteLine("Result: " + result);
            Console.WriteLine("Go done");
        }

        // optymalizacja
        class PageLoader
        {
            Dictionary<string, string> _cache = new Dictionary<string, string>();

            public async Task<string> LoadPage(string url)
            {
                if (_cache.ContainsKey(url))
                    return _cache[url];

                return _cache[url] = await new WebClient().DownloadStringTaskAsync(url);
            }
        }

        public async static Task LoadPages()
        {
            string url = "http://www.infotraining.pl";

            PageLoader loader = new PageLoader();

            var timer = Stopwatch.StartNew();

            string result = await loader.LoadPage(url);

            timer.Stop();

            Console.WriteLine(timer.ElapsedMilliseconds);

            Console.WriteLine("\n\n");

            timer.Restart();

            result = await loader.LoadPage(url);

            timer.Stop();

            Console.WriteLine(timer.ElapsedMilliseconds);
        }

        static void Main(string[] args)
        {
            var task = Go();
         
            DisplayPrimesCount();

            task.Wait();

            Console.ReadKey(true);

            LoadPages().Wait();
        }
    }
}
