﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAsyncMethods
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        // metoda synchroniczna
        //private void btnDownload_Click(object sender, RoutedEventArgs e)
        //{
        //    var request = (HttpWebRequest)WebRequest.Create(txtUrl.Text);
        //    request.Method = "HEAD";
        //    var response = (HttpWebResponse)request.GetResponse();
        //    string headerText = FormatHeader(response.Headers);
        //    txtContent.Text = headerText;
        //}


        // metoda asynchroniczna klasyczna
        //private void btnDownload_Click(object sender, RoutedEventArgs e)
        //{
        //    var sync = SynchronizationContext.Current;

        //    var request = (HttpWebRequest)WebRequest.Create(txtUrl.Text);
        //    request.Method = "HEAD";
        //    request.BeginGetResponse(
        //        asyncResult =>
        //        {
        //            var response = (HttpWebResponse)request.EndGetResponse(asyncResult);
        //            string headerText = FormatHeader(response.Headers);
        //            sync.Post(delegate { txtContent.Text = headerText; }, null);
        //        }, null);
        //}

        private async Task<string> DownloadHeadAsync(string url)
        {
            Debug.WriteLine("DownloadHeadAsync started... " + Thread.CurrentThread.ManagedThreadId);

            var request = (HttpWebRequest)WebRequest.Create(url);

            request.Method = "HEAD";

            await Task.Run(() =>
            {
                Debug.WriteLine("Fake task: " + Thread.CurrentThread.ManagedThreadId);
            });// .ConfigureAwait(continueOnCapturedContext: false);  // solution

            Debug.WriteLine("After fake task: " + Thread.CurrentThread.ManagedThreadId);

            HttpWebResponse response = (HttpWebResponse) await request.GetResponseAsync();

            Debug.WriteLine("DownloadHeadAsync after 1 await... " + Thread.CurrentThread.ManagedThreadId);

            string header = await FormatHeaderAsync(response.Headers);

            Debug.WriteLine("DownloadHeadAsync after 2 await... " + Thread.CurrentThread.ManagedThreadId);

            return header;
        }

        private Task<string> FormatHeaderAsync(WebHeaderCollection headers)
        {
            var formatTask = Task.Run(() =>
            {
                var headerStrings = from header in headers.Keys.Cast<string>()
                                    select string.Format("{0}: {1}", header, headers[header]);

                Thread.Sleep(3000);

                return string.Join(Environment.NewLine, headerStrings.ToArray());
            });

            return formatTask;
        }

        //private async void btnDownload_Click(object sender, RoutedEventArgs e)
        //{
        //    txtContent.Text = "Start downloading...";

        //    try
        //    {
        //        string headerText = await DownloadHeadAsync(txtUrl.Text);

        //        txtContent.Text = headerText;  // update UI
        //    }
        //    catch (Exception ex)
        //    {

        //        txtContent.Text = "An error occured: " + ex.Message;
        //    }            
        //}

        // DEADLOCK!!!
        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            txtContent.Text = "Start downloading...";
            Debug.WriteLine("Start downloading... " + Thread.CurrentThread.ManagedThreadId);
      
            try
            {
                string url = txtUrl.Text;
                string headerText = Task.Run(() => DownloadHeadAsync(url)).Result;

                txtContent.Text = headerText;  // update UI
            }
            catch (Exception ex)
            {
                
                txtContent.Text = "An error occured: " + ex.ToString();
            }
        }


    }
}
