﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;

namespace ExpressionTreesBasics
{

    public class MyService
    {
        public void Run()
        {
        }
    }
   
    [Subject(typeof(MyService))]
    class TestMyService
    {
        private Establish context = () =>
        {
            ms = new MyService();
        };

        Because of = () => { ms.Run(); };

        private It should_get_data_from_repo = () =>
        {
            /* */
        };

        private It should_calculate_result = () => { };

        private It should_cache_calculated_value;


        private static MyService ms;
    }


    [Subject("ExpressionTrees")]
    public class Expression_tree
    {
        protected static Expression<Func<string, bool>> _expression;

        private Establish context = () => { _expression = s => s.Length < 5; };

        private It should_assigned_to_lambda_expression =
            () => { _expression.NodeType.ShouldEqual(ExpressionType.Lambda); };

        private It should_have_one_input_parameter = () =>
        {
            var parameter = _expression.Parameters.First();

            parameter.NodeType.ShouldEqual(ExpressionType.Parameter);
            parameter.Name.ShouldEqual("s");
            parameter.Type.ShouldEqual(typeof(string));
        };

        private It should_have_less_than_expression_as_body =
            () => { _expression.Body.NodeType.ShouldEqual(ExpressionType.LessThan); };

        It should_be_compilable_to_delegate = () => { _expression.Compile().ShouldBeAssignableTo<Func<string, bool>>(); };
    }

    [Subject("ExpressionTrees")]
    public class Body_expression : Expression_tree
    {
        protected static Expression _bodyExpression;

        private Establish context = () => { _bodyExpression = _expression.Body; };

        private It should_be_binary_expression = () => { _bodyExpression.ShouldBeAssignableTo<BinaryExpression>(); };

        private It should_have_on_left_MemberAccess_expression = () =>
        {
            (_bodyExpression as BinaryExpression).Left.Equals(ExpressionType.MemberAccess);
        };

        private It should_have_on_right_Constant_expression = () =>
        {
            (_bodyExpression as BinaryExpression).Right.Equals(ExpressionType.Constant);
        };
    }

    [Subject("ExpressionTrees")]
    public class Member_expression : Body_expression
    {
        protected static MemberExpression _memberExpression;

        private Establish context =
            () => { _memberExpression = ((BinaryExpression) _bodyExpression).Left as MemberExpression; };

        private It should_store_member_name =
            () => { _memberExpression.Member.Name.ShouldEqual("Length"); };

        private It should_have_parameter =
            () => { _memberExpression.Expression.NodeType.ShouldEqual(ExpressionType.Parameter); };
    }

    [Subject("ExpressionTrees")]
    public class Parameter_expression : Member_expression
    {
        protected static ParameterExpression _parameterExpression;

        private Establish context = () => { _parameterExpression = _memberExpression.Expression as ParameterExpression; };

        private It should_have_name =
             () => { _parameterExpression.Name.ShouldEqual("s"); };

        private It should_be_string =
            () => {
                _parameterExpression.Type.ShouldEqual(typeof(string));
            };
    }

    [Subject("ExpressionTrees")]
    public class Constant_expression : Body_expression
    {
        private static ConstantExpression _constantExpression;

        Establish context = () => { _constantExpression = ((BinaryExpression)_bodyExpression).Right as ConstantExpression; };

        private It should_store_value = () => { _constantExpression.Value.ShouldEqual(5); };        
    }

    [Subject("ExpressionTrees")]
    public class Compiled_to_delegate : Expression_tree
    {
        protected static Func<string, bool> _compiled;

        Establish context = () => { _compiled = _expression.Compile(); };
    }

    [Subject("ExpressionTrees")]
    public class ExecutingWithShortString : Compiled_to_delegate
    {
        private Because of = () => { _result = _compiled("short"); };

        private It should_return_false = () => { _result.ShouldBeFalse(); };

        private static bool _result;
    }

    [Subject("ExpressionTrees")]
    public class ExecutingWithLongString : Compiled_to_delegate
    {
        private Because of = () => { _result = _compiled("very long"); };

        private It should_return_false = () => { _result.ShouldBeFalse(); };

        private static bool _result;
    }
}