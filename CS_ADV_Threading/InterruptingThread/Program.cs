﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace InterruptingThread
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread thd = new Thread(() =>
            {
                try
                {
                    Console.WriteLine("Zasypiam");
                    Thread.Sleep(Timeout.Infinite);
                }
                catch (ThreadInterruptedException)
                {
                    Console.Write("Wymuszone ");
                }
                Console.WriteLine("przebudzenie!");
            });

            thd.Start();

            Thread.Sleep(TimeSpan.FromSeconds(5));

            thd.Interrupt();

            Console.ReadKey();
        }
    }

}
