﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadDemo
{
    class Program
    {
        private static volatile bool isRunning = true;
        private static int data;

        static void WriteChar(char c)
        {
            Console.WriteLine("Start wątku: {0}", Thread.CurrentThread.Name);

            while(isRunning)
            {
               Console.Write(c);
               Thread.Sleep(100);
            }         
            
            Console.WriteLine("\n\nData: " + data);     
        }

        static void Main(string[] args)
        {
            Thread thd1 = new Thread(() => WriteChar('X'));
            thd1.IsBackground = true;
            thd1.Name = "THD_X1";
            thd1.Start();

            //WriteChar('O');
            Console.ReadKey();

            // code

            data = 13;

            isRunning = false;

            // code

            thd1.Join();
        }
    }
}
