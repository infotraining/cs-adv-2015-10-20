﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace AsynchronousDelegetes
{
    class Program
    {
        static int PerformLongCalc(int[] data)
        {
            int result = data.Sum();

            // symulacja czasochłonnej operacji
            Thread.Sleep(3000);

            return result;
        }

        // delegat
        delegate int LongOperationDelegate(int[] data);

        static void Main(string[] args)
        {
            LongOperationDelegate function = PerformLongCalc;

            int[] myData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            IAsyncResult asyncRslt = function.BeginInvoke(myData, null, null);

            Console.WriteLine("Doing more work on Main thread.");

            int result = function.EndInvoke(asyncRslt);

            Console.WriteLine("Result: {0}", result);
        }
    }
}
