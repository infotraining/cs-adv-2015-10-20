﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WaitPulseQueue
{

    class WaitPulseQ
    {
        Queue<string> _queue = new Queue<string>();
        private object _locker = new Object();

        public void Enqueue(string item)
        {
            lock (_locker)
            {
                _queue.Enqueue(item);

                Monitor.Pulse(_locker);
            }
        }

        public string Dequeue()
        {
            lock (_locker)
            {
                if (_queue.Count == 0)
                {
                    Monitor.Wait(_locker);
                }

                string item = _queue.Dequeue();

                return item;
            }
        }
    }



    class Program
    {
        static WaitPulseQ q = new WaitPulseQ();

        static void Worker()
        {
            Thread.Sleep(5000);
            q.Enqueue("text");
        }

        static void Main(string[] args)
        {
            Task.Run(() => Worker());

            string item = q.Dequeue();

            Console.WriteLine(item);
        }
    }
}
