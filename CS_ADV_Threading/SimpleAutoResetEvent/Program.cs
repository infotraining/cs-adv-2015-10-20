﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SimpleAutoResetEvent
{
    class Program
    {
        static EventWaitHandle wh = new AutoResetEvent(false);

        static void WorkerThread()
        {
            Console.WriteLine("Ready...");
            wh.WaitOne();
            Console.WriteLine("Job has been started...");
            Thread.Sleep(5000);
            Console.WriteLine("Job has been finished...");
        }

        static void Main(string[] args)
        {
            Thread thd = new Thread(WorkerThread);
            thd.Start();
            
            Console.WriteLine("Press ENTER to start a job...");
            Console.ReadLine();

            wh.Set();
        }
    }

}
