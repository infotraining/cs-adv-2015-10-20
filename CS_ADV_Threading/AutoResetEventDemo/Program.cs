﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutoResetEventDemo
{
    class Program
    {
        static EventWaitHandle _waitHandle = new ManualResetEvent(false);

        static void Worker(int id)
        {
            Console.WriteLine("Thd#" + id + " has started...");

            _waitHandle.WaitOne();

            Console.WriteLine("Thd#" + id + " is running...");
        }

        static void Main(string[] args)
        {
            Thread thd = new Thread(() => Worker(1));
            thd.Start();

            Task tsk = Task.Run(() => Worker(2));

            Thread.Sleep(5000);

            Console.WriteLine("Event occured...");

            _waitHandle.Set();

            thd.Join();

            //Thread.Sleep(5000);
           
            tsk.Wait();
        }
    }
}
