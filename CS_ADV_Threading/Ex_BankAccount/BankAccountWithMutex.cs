﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BankAccount
{
    public class BankAccountWithMutex
    {
        private decimal _balance;
        private Mutex _mtx = new Mutex();

        public BankAccountWithMutex(int id, decimal balance)
        {
            Id = id;
            Balance = balance;
        }

        public int Id { get; private set; }

        public decimal Balance
        {
            get
            {
                try
                {
                    _mtx.WaitOne();
                    return _balance;
                }
                finally
                {
                    _mtx.ReleaseMutex();
                }
            }

            private set { _balance = value; }
        }

        public void Deposit(decimal amount)
        {
            try
            {
                _mtx.WaitOne();
                _balance += amount;
            }
            finally
            {
                _mtx.ReleaseMutex();
            }
        }

        public void Withdraw(decimal amount)
        {
            try
            {
                _mtx.WaitOne();
                _balance -= amount;
            }
            finally
            {
                _mtx.ReleaseMutex();
            }
        }

        public void Transfer(BankAccountWithMutex to, decimal amount)
        {
            Mutex[] mutexes = { _mtx, to._mtx };

            if (Mutex.WaitAll(mutexes))
            {
                try
                {
                    Console.WriteLine("Transfering funds account1 #{0} -> #{1}", Id, to.Id);

                    _balance -= amount;
                    to._balance += amount;
                }
                finally
                {
                    foreach (var m in mutexes)
                    {
                        m.ReleaseMutex();
                    }
                }
            }

        }
    }
}
