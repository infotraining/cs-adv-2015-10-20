using System;

namespace BankAccount
{
    class BankAccount
    {
        private decimal _balance;
        private object _locker = new object();

        public BankAccount(int id, decimal balance)
        {
            Id = id;
            Balance = balance;
        }

        public int Id { get; private set; }

        public decimal Balance
        {
            get
            {
                lock (_locker)
                    return _balance;
            }

            private set { _balance = value; }
        }

        public void Deposit(decimal amount)
        {
            lock (_locker)
            {
                Balance += amount;
            }
        }

        public void Withdraw(decimal amount)
        {
            lock (_locker)
            {
                Balance -= amount;
            }
        }

        public void Transfer(BankAccount to, decimal amount)
        {
            object firstLock;
            object secondLock;

            ChooseLocks(this, to, out firstLock, out secondLock);

            lock (firstLock)
            {
                Console.WriteLine("Transfering funds account1 #{0} -> #{1}", Id, to.Id);

                lock (secondLock)
                {
                    _balance -= amount;
                    to._balance += amount;
                }
            }
        }

        private static void ChooseLocks(BankAccount account1, BankAccount account2, 
                                        out object lk1, out object lk2)
        {
            if (account1.Id < account2.Id)
            {
                lk1 = account1._locker;
                lk2 = account2._locker;
            }
            else
            {
                lk1 = account2._locker;
                lk2 = account1._locker;
            }
        }
    }
}