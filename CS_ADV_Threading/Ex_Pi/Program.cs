﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ex_Pi
{
    class Program
    {
        const long N = 300000000;

        static void SingleThreadPi()
        {
            Console.WriteLine("SingleThreadedPi started...");
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;
            Random random = new Random(Guid.NewGuid().GetHashCode());

            for (long i = 0; i < N; ++i)
            {

                double x = random.NextDouble() * 2 - 1.0;
                double y = random.NextDouble() * 2 - 1.0;

                double length = Math.Sqrt(x * x + y * y);

                if (length < 1.0)
                    ++counter;
            }

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void ThreadPiWithInterlocked()
        {
            Console.WriteLine("ThreadUnsafePi started...");

            int noOfThreads = Environment.ProcessorCount;
            Console.WriteLine("No of threads: {0}", noOfThreads);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;

            List<Thread> threads = new List<Thread>();
            long throwsPerThread = N / noOfThreads;


            for (int i = 0; i < noOfThreads; ++i)
            {
                var thd = new Thread(() =>
                {
                    Random random = new Random(Guid.NewGuid().GetHashCode());

                    for (long j = 0; j < throwsPerThread; ++j)
                    {
                        double x = random.NextDouble() * 2 - 1.0;
                        double y = random.NextDouble() * 2 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                            Interlocked.Increment(ref counter);
                    }
                });

                thd.Start();
                threads.Add(thd);
            }

            foreach (var t in threads)
                t.Join();

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void ThreadPiWithLock()
        {
            Console.WriteLine("ThreadUnsafePi started...");

            int noOfThreads = Environment.ProcessorCount;
            Console.WriteLine("No of threads: {0}", noOfThreads);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;
            object locker = new object();

            List<Thread> threads = new List<Thread>();
            long throwsPerThread = N / noOfThreads;


            for (int i = 0; i < noOfThreads; ++i)
            {
                var thd = new Thread(() =>
                {
                    Random random = new Random(Guid.NewGuid().GetHashCode());

                    for (long j = 0; j < throwsPerThread; ++j)
                    {
                        double x = random.NextDouble() * 2 - 1.0;
                        double y = random.NextDouble() * 2 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                        {
                            lock (locker)
                            {
                                ++counter;
                            }
                        }

                    }
                });

                thd.Start();
                threads.Add(thd);
            }

            foreach (var t in threads)
                t.Join();

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void ThreadPiWithoutLock1()
        {
            Console.WriteLine("ThreadPiWithoutLock1 started...");

            int noOfThreads = Environment.ProcessorCount;
            Console.WriteLine("No of threads: {0}", noOfThreads);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;
            object locker = new object();

            List<Thread> threads = new List<Thread>();
            long throwsPerThread = N / noOfThreads;


            for (int i = 0; i < noOfThreads; ++i)
            {
                var thd = new Thread(() =>
                {
                    Random random = new Random(Guid.NewGuid().GetHashCode());

                    long localCounter = 0;
                    for (long j = 0; j < throwsPerThread; ++j)
                    {
                        double x = random.NextDouble() * 2 - 1.0;
                        double y = random.NextDouble() * 2 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                        {
                            ++localCounter;
                        }
                    }

                    lock (locker)
                        counter += localCounter;

                });

                thd.Start();
                threads.Add(thd);
            }

            foreach (var t in threads)
                t.Join();

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void ThreadPiWithoutLock2()
        {
            Console.WriteLine("ThreadPiWithoutLock2 started...");

            int noOfThreads = Environment.ProcessorCount;
            Console.WriteLine("No of threads: {0}", noOfThreads);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;
            object locker = new object();

            List<Thread> threads = new List<Thread>();
            long[] results = new long[noOfThreads];
            long throwsPerThread = N / noOfThreads;


            for (int i = 0; i < noOfThreads; ++i)
            {
                int index = i;
                var thd = new Thread(() =>
                {
                    Random random = new Random(Guid.NewGuid().GetHashCode());

                    long localCounter = 0;
                    for (long j = 0; j < throwsPerThread; ++j)
                    {
                        double x = random.NextDouble() * 2 - 1.0;
                        double y = random.NextDouble() * 2 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                        {
                            ++localCounter;
                        }
                    }

                    results[index] = localCounter;
                });

                thd.Start();
                threads.Add(thd);
            }

            foreach (var t in threads)
                t.Join();

            counter = results.Sum();

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void ThreadPiWithoutLock3()
        {
            Console.WriteLine("ThreadPiWithoutLock3 started...");

            int noOfThreads = Environment.ProcessorCount;
            Console.WriteLine("No of threads: {0}", noOfThreads);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;
            object locker = new object();

            List<Thread> threads = new List<Thread>();
            long[] results = new long[noOfThreads];
            long throwsPerThread = N / noOfThreads;


            for (int i = 0; i < noOfThreads; ++i)
            {
                int index = i;
                var thd = new Thread(() =>
                {
                    Random random = new Random(Guid.NewGuid().GetHashCode());

                    for (long j = 0; j < throwsPerThread; ++j)
                    {
                        double x = random.NextDouble() * 2 - 1.0;
                        double y = random.NextDouble() * 2 - 1.0;

                        double length = Math.Sqrt(x * x + y * y);

                        if (length < 1.0)
                        {
                            results[index]++;
                        }
                    }
                });

                thd.Start();
                threads.Add(thd);
            }

            foreach (var t in threads)
                t.Join();

            counter = results.Sum();

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void ParallelForPi()
        {
            Console.WriteLine("ParallelForPi started...");
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            long counter = 0;
            object locker = new object();

            ThreadLocal<Random> randomGen 
                = new ThreadLocal<Random>(() => new Random(Guid.NewGuid().GetHashCode()));

            Parallel.For(0, (int)N, () => 0, (i, loopState, localCounter)
               =>
           {
               var random = randomGen.Value;
               double x = random.NextDouble() * 2 - 1.0;
               double y = random.NextDouble() * 2 - 1.0;

               double length = Math.Sqrt(x * x + y * y);

               if (length < 1.0)
                   return localCounter + 1;
               return localCounter;
           },
                (localCounter) =>
                {
                    lock (locker)
                    {
                        counter += localCounter;
                    }
                });

            double pi = ((double)counter / N) * 4;

            stopwatch.Stop();

            Console.Write("PI = {0}; ", pi);

            Console.WriteLine("Time:  {0}s", stopwatch.Elapsed);
        }

        static void Main(string[] args)
        {
            //SingleThreadPi();

            //Console.WriteLine();

            //ThreadPiWithInterlocked();

            //Console.WriteLine();

            //ThreadPiWithLock();

            //Console.WriteLine();

            ThreadPiWithoutLock1();

            Console.WriteLine();

            Thread.Sleep(1000);

            //ThreadPiWithoutLock2();

            //Console.WriteLine();

            //ThreadPiWithoutLock3();

            //Console.WriteLine();

            ParallelForPi();
        }
    }
}
