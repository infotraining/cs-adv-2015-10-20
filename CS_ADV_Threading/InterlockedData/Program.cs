﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace InterlockedData
{
    class Program
    {
        static int nextId = 0;
        static long counter = 1000000;
        static object locker = new object();

        static void Reset()
        {
            nextId = 0;
        }

        static void DoCalcUnsafe()
        {
            for (int i = 0; i < counter; ++i)
            {   
                nextId++;
            }
        }

        static void DoCalcSafeWithLock()
        {
            for(int i = 0; i < counter; ++i)
                lock (locker)
                {
                    nextId++;
                }
        }

        static void DoCalcSafeWithInterlocked()
        {
            for (int i = 0; i < counter; ++i)
                Interlocked.Increment(ref nextId);
        }

        static void Main(string[] args)
        {
            Thread[] threads = { new Thread(DoCalcUnsafe), new Thread(DoCalcUnsafe) };
            foreach (Thread t in threads)
                t.Start();
            foreach (Thread t in threads)
                t.Join();

            Console.WriteLine("With unsafe threads: {0}", nextId);

            Reset();

            threads[0] = new Thread(DoCalcSafeWithLock);
            threads[1] = new Thread(DoCalcSafeWithLock);
            foreach (Thread t in threads)
                t.Start();
            foreach (Thread t in threads)
                t.Join();

            Console.WriteLine("With locked threads: {0}", nextId);

            Reset();

            threads[0] = new Thread(DoCalcSafeWithInterlocked);
            threads[1] = new Thread(DoCalcSafeWithInterlocked);
            foreach (Thread t in threads)
                t.Start();
            foreach (Thread t in threads)
                t.Join();

            Console.WriteLine("With locked threads: {0}", nextId);
        }
    }
}
