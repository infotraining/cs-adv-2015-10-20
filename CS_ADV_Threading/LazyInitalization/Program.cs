﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LazyInitalization
{
    class Expensive
    {
        public Expensive()
        {
            Console.WriteLine("Creating an expensive object...");
        }

        public void Process()
        {
            Console.WriteLine("Expensive object is working...");
        }
    }

    class Broker
    {
        #region Thread-Safe implementation of lazy initialization 
        private Expensive _expensive;
        readonly object _expensiveLock = new object();

        public Expensive Item1
        {
            get
            {
                lock (_expensiveLock)
                {
                    if (_expensive == null)
                        _expensive = new Expensive();
                    return _expensive;
                }
            }
        }
        #endregion

        #region Thread-Safe implementation using Lazy<T>
        Lazy<Expensive> _lazyExpensive = new Lazy<Expensive>(() => new Expensive(), true);

        public Expensive Item2 
        {
            get
            {
                return _lazyExpensive.Value;
            }
        }
        #endregion
    }


    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creating a broker...");
            Broker broker = new Broker();

            Console.ReadKey(true);

            broker.Item1.Process();

            Console.ReadKey(true);

            broker.Item2.Process();
        }
    }
}
