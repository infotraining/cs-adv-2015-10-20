﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleCountdownEvent
{
    class Program
    {
        static CountdownEvent countdown = new CountdownEvent(3);

        static void Main(string[] args)
        {
            new Thread(() => { WorkerThread("One", 1000); }).Start();
            new Thread(() => { WorkerThread("Two", 2000); }).Start();
            new Thread(() => { WorkerThread("Three", 3000); }).Start();

            countdown.Wait();

            Console.WriteLine("All threads have finished processing...");

        }

        static void WorkerThread(string text, int sleepTime)
        {
            Console.WriteLine("Processing... {0}", text);
            Thread.Sleep(sleepTime);
            countdown.Signal();
        }
    }
}
