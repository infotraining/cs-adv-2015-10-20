﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CancellationTokens
{
    class Program
    {

        static void Work(CancellationToken canceler)
        {
            int i = 0;

            while (true)
            {
                Console.WriteLine("Thread is working... {0}", ++i);
                canceler.ThrowIfCancellationRequested();
                Thread.Sleep(1000);
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Press key to start and another key to cancel a task...");

            var cancelSource = new CancellationTokenSource();
            
            Thread thd = new Thread(() =>
            {
                try
                {
                    Work(cancelSource.Token);
                }
                catch (OperationCanceledException ex)
                {
                    Console.WriteLine("Operation has been cancelled...");
                }
            });

            Console.ReadKey(true);

            thd.Start();

            Console.ReadKey(true);

            cancelSource.Cancel();
        }
    }
}
