﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToDataSet
{
    class Program
    {
        static DataSet dsNorthwind = new DataSet("Northwind");

        static void LoadDataFromDB(string tableName, string query, SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);

            adapter.Fill(dsNorthwind, tableName);
        }

        static void PrintTable(string tableName)
        {
            Console.WriteLine("\n" + tableName + ":");
            Console.WriteLine("---------------------------------------------------------------------");
            foreach (DataRow row in dsNorthwind.Tables[tableName].Rows)
            {
                for (int i = 0; i < dsNorthwind.Tables[tableName].Columns.Count; i++)
                    Console.Write(string.Format("{0,32}|", row[i]));
                Console.WriteLine();
            }
        }

        static void PrintProductsStartingWith(string pattern)
        {
            Console.WriteLine("\nProducts starting with '{0}':", pattern);
            Console.WriteLine("---------------------------------------------------------------------");
            var products = from r in dsNorthwind.Tables["Products"].AsEnumerable()
                           where (r["ProductName"] as string).StartsWith(pattern)
                           select new
                           {
                               ProductName = r["ProductName"] as string,
                               UnitPrice = r.Field<decimal>("UnitPrice"),
                               QuantityPerUnit = r.Field<string>("QuantityPerUnit")
                           };

            foreach (var p in products)
                Console.WriteLine("{0,-30} {1,10:c} {2,25}", p.ProductName, p.UnitPrice, p.QuantityPerUnit);
        }

        static void CreateTableWithExpensiveProducts(decimal priceThreshold)
        {
            IEnumerable<DataRow> expensiveProducts
                = from r in dsNorthwind.Tables["Products"].AsEnumerable()
                  where r.Field<decimal>("UnitPrice") > priceThreshold
                  select r;

            DataTable table = expensiveProducts.CopyToDataTable<DataRow>();
            dsNorthwind.Tables.Add(table);
            dsNorthwind.Tables[1].TableName = "ExpensiveProducts";
        }

        static void Main(string[] args)
        {
            Console.WriteLine("LinqToDataSet demo:\n");

            SqlConnectionStringBuilder connStringBld = new SqlConnectionStringBuilder();
            connStringBld.DataSource = @"(LocalDB)\MSSQLLocalDB";
            connStringBld.AttachDBFilename = @"|DataDirectory|\northwind.mdf";
            connStringBld.IntegratedSecurity = true;

            try
            {
                SqlConnection conn = new SqlConnection(connStringBld.ConnectionString);

                LoadDataFromDB("Products", "SELECT [ProductName], [UnitPrice], [QuantityPerUnit] FROM Products", conn);
                PrintTable("Products");
                PrintProductsStartingWith("C");
                CreateTableWithExpensiveProducts(75);
                PrintTable("ExpensiveProducts");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
