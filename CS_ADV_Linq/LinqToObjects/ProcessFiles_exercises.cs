﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;

namespace Exercise_LinqToObjects
{
    public class ProcessFiles_exercises
    {
        FileInfo[] files;

        public ProcessFiles_exercises()
        {
            using (Stream stream = new FileStream("files.dat", FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                files = (FileInfo[])formatter.Deserialize(stream);
            }
        }


        // UWAGA! Ignoruj wielkość liter w nazwach plików

        [Fact]
        void FindDllFilesInAlphabeticalOrder()
        {
            // 1 - znajdz wszystkie nazwy plików z rozszerzeniem dll w kolejnosci alfabetycznej 
            var dllFiles = files.Where(fi => string.Equals(fi.Extension, ".dll", StringComparison.InvariantCultureIgnoreCase)).Select(fi => fi.Name.ToLower()).OrderBy(n => n);

            dllFiles.Count().Should().Be(2277);
            dllFiles.Should().BeInAscendingOrder();
            dllFiles.All(n => n.Split('.').Last() == "dll").Should().BeTrue();

            dllFiles.Count().Should().Be(2277);
            dllFiles.Should().BeInAscendingOrder();
            dllFiles.All(n => n.Split('.').Last() == "dll").Should().BeTrue();
        }

        [Fact]
        void CountNumberOfExeFiles()
        {
            // 2 - znajdz liczbę plików z rozszerzeniem .exe    
            var noOfExeFiles = files.Count(fi => string.Equals(fi.Extension, ".exe", StringComparison.InvariantCultureIgnoreCase));

            noOfExeFiles.Should().Be(319);
        }

        [Fact]
        void IsAnyFileWithDate2011()
        {
            // 3 - znajdz czy są jakiekolwiek pliki z datą utworzenia 2011
            var isAnyFileFrom2011 = files.Any(fi => fi.CreationTime.Year == 2011);

            isAnyFileFrom2011.Should().BeTrue();
        }

        [Fact]
        void GroupByExtensionAndCalculateStats()
        {
            // 4 - pogrupuj pliki  wg rozszerzenia i znajdz ile jest plikow z danym rozszerzeniem
            //     posortuj grupy alfabetycznie wg rozszerzeń

            var statFiles =
                files.GroupBy(f => f.Extension.ToLower(), 
                              f => f, (key, fs) => new {Extension = key, Count = fs.Count()})
                      .OrderBy(r => r.Extension);
                    
            statFiles.Count().Should().Be(47);
            statFiles.Should().BeInAscendingOrder(g => g.Extension);
            statFiles.First(g => g.Extension == ".exe").Count.Should().Be(319);
            statFiles.First(g => g.Extension == ".dll").Count.Should().Be(2277);
        }

        [Fact]
        void CreateDictionaryWithFirstLetterAsKeyAndFileCountAsValue()
        {
            // 5 - pogrupuj wg pierwszej litery w nazwie pliku i utwórz słownik zawierający 5 najczęsciej występujących liter i ilość plików w grupie

            var result = (from file in files
                          group file by file.Name.ToLower()[0]
                          into groupedByFirstLetter
                          orderby groupedByFirstLetter.Count() descending
                          select groupedByFirstLetter).Take(5).ToDictionary(g => g.Key, g => g.Count());

            var expected = new Dictionary<char, int> { { 'm', 379 }, { 'w', 350 }, { 'c', 289 }, { 'k', 225 }, { 's', 216 } };

            result.Should().Equal(expected);
        }
    }
}
