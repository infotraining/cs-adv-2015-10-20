﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Exercise_LinqToXml
{
    class Program
    {
        static void Main(string[] args)
        {
            WebClient client = new WebClient();
            string kursyTabelaA = client.DownloadString(@"http://rss.nbp.pl/kursy/TabelaA.xml");

            File.WriteAllText("kursyTabelaA.xml", kursyTabelaA);

            var xmlKursy = XElement.Parse(kursyTabelaA);

            var tabela = xmlKursy.Descendants("item").Select(item => new { Title = item.Element("title").Value, Url = item.Element("enclosure").Attribute("url").Value }).First();

            Console.WriteLine(tabela.Title + " - url: " + tabela.Url);

            string[] koszykWalut = { "USD", "GBP", "EUR", "CAD" };

            // wyswietl dane z koszyka w formacie: Kod waluty: {0,3}; Nazwa waluty: {1,-20}; Przelicznik: {2,-3}; Kurs średni:  {3:f4}
            string tabelaKursow = client.DownloadString(tabela.Url);

            var xmlTabela = XElement.Parse(tabelaKursow);

            var kursyWalut = from item in xmlTabela.Elements("pozycja")
                             where koszykWalut.Contains(item.Element("kod_waluty").Value)
                             select new
                             {
                                 Kod = item.Element("kod_waluty").Value,
                                 Nazwa = item.Element("nazwa_waluty").Value,
                                 Przelicznik = item.Element("przelicznik").Value,
                                 KursSredni = decimal.Parse(item.Element("kurs_sredni").Value)
                             };

            foreach (var kurs in kursyWalut)
                Console.WriteLine("Kod waluty: {0,3}; Nazwa waluty: {1,-20}; Przelicznik: {2,-3}; Kurs średni: {3:f4}",
                    kurs.Kod, kurs.Nazwa, kurs.Przelicznik, kurs.KursSredni);
        }

    }
}
